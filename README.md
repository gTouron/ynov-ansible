# READ

use aws configure to set up a profile
export AWS_PROFILE=profile_name

terraform init
terraform plan
terraform apply

in the terraform folder use this cmd `terraform output -raw private_key > ../generated.pem`
change the permissions of the pem file `chmod 400 ../generated.pem`
change the host in the ansible inventory file to the public ip of the instance

ansible-playbook -i inventory.yml playbook.yml
